const getAuthenticated = ({authState}) =>
  authState.authenticated;

const getDisplayName = ({authState}) =>
  authState.displayName;

const getEmail = ({authState}) => 
  authState.email;

const getUID = ({authState}) =>
  authState.uid;

const getMetadata = ({authState}) =>
  authState.metadata;

const getPhotoURL = ({authState}) =>
  authState.photoURL;

const getCurrentUser = ({authState}) =>
  authState;

const getFirebaseAuth = ({firebase}) => 
  firebase.auth;

const getFirebaseProfile = ({firebase}) =>
  firebase.profile;

const getFirebaseUID = ({firebase}) => 
  firebase.auth.uid;

const getFirebaseCurrentUser = ({firebase}) => 
  firebase.auth;

const getFirebaseCurrentUserProfile = ({firebase}) => ({
  ...firebase.profile,
  uid: firebase.auth.uid
});
  

export {
  getAuthenticated,
  getDisplayName,
  getEmail,
  getUID,
  getMetadata,
  getPhotoURL,
  getCurrentUser,
  getFirebaseAuth,
  getFirebaseProfile,
  getFirebaseUID,
  getFirebaseCurrentUser,
  getFirebaseCurrentUserProfile
}