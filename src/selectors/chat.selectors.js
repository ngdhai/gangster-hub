const getChatLog = ({chatState}) => chatState.chatLog;

const getChatLogLength = ({chatState}) => chatState.chatLog ? chatState.chatLog.length : 0

export {
  getChatLog,
  getChatLogLength
}