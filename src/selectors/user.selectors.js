const getSelectedUser = ({userState}) => 
  userState.selected;



const getUsersBaseOnFilter = ({userState}, {userFilter}) =>{
  return userState.users.filter(user => user.displayName.toLowerCase().includes(userFilter.toLowerCase()));
}

export {
  getSelectedUser,
  getUsersBaseOnFilter
}