import { 
  USER_SELECT,
  USER_ADD
} from '../../actions/types';

const INITIAL_STATE = {
  users: [],
};

const applyUserSelect = (state, {payload}) => {
  const users = state.users.map(user => ({
    ...user,
    selected: user.uid === payload.uid && !user.selected 
  }))
  return {
    users,
    selected: users.filter(user => user.selected)[0]
  };
};

const applyUserAdd = (state, {payload}) => ({
  selected: state.selected,
  ...payload
});

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case USER_SELECT: {
      return applyUserSelect(state, action);
    }
    case USER_ADD: {
      return applyUserAdd(state, action);
    }
    default: return state;
  }
};