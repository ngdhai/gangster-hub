import { 
  AUTH_AUTHENTICATED,
  AUTH_UNAUTHENTICATED,
} from '../../actions/types';

const INITIAL_STATE = {
  authenticated: false,
};

const applyAuthenticated = (state, action) => ({
  ...action.payload
});

const applyUnAuthenticated = (state, action) => ({
  ...action.payload
});

export default (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case AUTH_AUTHENTICATED: {
      return applyAuthenticated(state, action);
    }
    case AUTH_UNAUTHENTICATED: {
      return applyUnAuthenticated(state, action);
    }
    default: return state;
  }
};