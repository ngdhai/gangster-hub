import { combineReducers } from 'redux';
import { firebaseReducer } from 'react-redux-firebase';
import authReducer from './auth/auth.reducer';
import userReducer from './user/user.reducer';
import chatReducer from './chat/chat.reducer';


export default combineReducers({
  authState: authReducer,
  userState: userReducer,
  chatState: chatReducer,
  firebase: firebaseReducer
});