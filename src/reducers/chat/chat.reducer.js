import { CHAT_CHATLOGADD } from "../../actions/types";

const INITIAL_STATE = {};


const applyChatLogAdd = (state, { payload }) => {
  const keys = Object.keys(payload.chatLog);
  
  const t1 = keys[0] ? Object.keys(payload.chatLog[keys[0]]).map(timeStamp => ({
    uid: keys[0],
    timeStamp,
    message: payload.chatLog[keys[0]][timeStamp]
  })) : [];
  const t2 = keys[1] ? Object.keys(payload.chatLog[keys[1]]).map(timeStamp => ({
    uid: keys[1],
    timeStamp,
    message: payload.chatLog[keys[1]][timeStamp]
  })) : [];

  const chatLog = [...t1, ...t2];

  //  console.log(temp);
  return {
    chatLog
  };
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHAT_CHATLOGADD: {
      return applyChatLogAdd(state, action);
    }
    default:
      return state;
  }
};
