import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header, Main, Footer } from './layouts';

import { doAuthFetchUser } from './actions/auth';
import { doUserFetchSignIn } from './actions/user';
import { auth, database } from 'firebase';

class App extends Component {

  render() {

    return (
      <Router>
        <>
          <Header className='header-div'/>

          <Main className='main-div'/>

          <Footer className='footer-div'/>
        </>
      </Router>
      
    );
  }

  componentWillMount() {
    this.props.fetchUser(auth());
    this.props.onRequestData(database());
  }
}

const mapDispatchToProps = dispatch => ({
  fetchUser: authRef => dispatch(doAuthFetchUser(authRef)),
  onRequestData: databaseRef => dispatch(doUserFetchSignIn(databaseRef))
})

export default connect(undefined, mapDispatchToProps)(App);
