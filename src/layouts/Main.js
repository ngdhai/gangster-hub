import React from 'react';
import { Route } from 'react-router-dom';
import { ChatBox, Login, SignUp } from '../components';

export default ({className}) =>
  <div className={className}>
      <Route path='/' exact component={ChatBox}/>
      <Route path='/login' component={Login}/>
      <Route path='/signup' component={SignUp}/>
  </div>
  
