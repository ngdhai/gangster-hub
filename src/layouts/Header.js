import React from 'react';
import { NavigationBar } from '../components';

export default ({ className }) =>
  <div className={className}>
    <NavigationBar title='GangsterHub'/>
  </div>