import React, { Component } from 'react';
import { connect } from 'react-redux';
import { doUserStar, doUserUnStar } from '../../actions/user';
import StatusBarPresenter from './status-bar.presenter';
import { getChatLogLength } from '../../selectors/chat.selectors';

class StatusBar extends Component {
  onStar = () => {
    const { onUserStar, user, currentUser } = this.props;
    onUserStar(currentUser, user)
  }

  onUnStar = () => {
    const { onUserUnStar, user, currentUser } =  this.props;
    onUserUnStar(currentUser, user);
  }

  render() {
    const { user, currentUser, chatLength } = this.props;
    const stared = currentUser.starUser ? Object.keys(currentUser.starUser).includes(user.uid) : false;
    return <StatusBarPresenter user={user} onStar={this.onStar} stared={stared} onUnStar={this.onUnStar} chatLength={chatLength}/>
  }
};

const mapStateToProps = state => ({
  chatLength: getChatLogLength(state)
});

const mapDispatchToProps = dispatch => ({
  onUserStar: (currentUser, user) => dispatch(doUserStar(currentUser, user)),
  onUserUnStar: (currentUser, user) => dispatch(doUserUnStar(currentUser, user))
});

export default connect(mapStateToProps, mapDispatchToProps)(StatusBar);