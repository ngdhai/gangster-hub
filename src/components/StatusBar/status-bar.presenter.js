import React from 'react';
import {
  Paper,
  Grid,
  Avatar,
  Typography
} from '@material-ui/core';
import { 
  Star, 
  StarBorder 
} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';

import styles from './status-bar.styles';

export default withStyles(styles)(({classes, user, onStar,onUnStar, stared, chatLength}) => 
  <Paper square={true} className={classes.statusBar}>
    {
      user ?
      <Grid container className={classes.header}>
        <Grid item xs={2}>
          <Grid container justify='center'>
            <Avatar 
              alt='User Avatar' 
              src={user.avatarUrl}
            />
          </Grid>
        </Grid>
        <Grid item xs={9}>
          <Typography variant='body2' className={classes.statusTypo}>
            Chatting with {user.displayName}
          </Typography>
          <Typography variant='body2' className={classes.messageTypo}>
            Already { chatLength } messages
          </Typography>
        </Grid>
        <Grid item xs={1}>
          {
            stared ? <Star className={classes.star} onClick={onUnStar}/> : <StarBorder className={classes.star} onClick={onStar}/>
          }
        </Grid>
      </Grid>
      : 
      null
    }
  </Paper>
);