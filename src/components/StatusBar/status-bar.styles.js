import { blueGrey } from '@material-ui/core/colors';

const styles = theme => ({
  statusBar: {
    height: 105,
  },
  header: {
    height: 105,
    backgroundColor: '#F2F5F8',
    padding: '32px 10px 10px 10px'
  },
  statusTypo: {
    color: blueGrey[800]
  },
  messageTypo: {
    fontSize: 11,
    color: blueGrey[200],
    fontWeight: 500
  },
  star: {
    cursor: 'pointer',
    marginTop: 5
  }
});

export default styles;