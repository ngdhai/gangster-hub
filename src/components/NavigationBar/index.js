import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withFirebase } from 'react-redux-firebase';
import { 
  // getAuthenticated,
  // getDisplayName,
  // getEmail,
  // getUID,
  // getMetadata,
  // getPhotoURL,
  getFirebaseUID,
  getFirebaseProfile,
  getFirebaseAuth
} from '../../selectors/auth.selectors';
import { doAuthLogout } from '../../actions/auth';

import NavigationBarPresenter from './navigation-bar.presenter';

class NavigationBar extends Component {

  handleLogin = () => {
    const { history } = this.props;
    history.push('/login');
  }

  handleSignup = () => {
    const { history } = this.props;
    history.push('/signup');
  }

  handleProfile = () => {
    console.log('Go to profifle');
  }

  handleLogout = async () => {
    const { history, firebase, uid } = this.props;
    if(await firebase.logout()) {
      await firebase.database().ref(`users/${uid}`).update({
        logoutTime: firebase.database.ServerValue.TIMESTAMP
      });
      await firebase.database().ref(`presence/`).update({
        [uid]: null
      });
      history.push('login');
    }
  }

  handleTitle = () => {
    const { history } = this.props;
    history.push('/');
  }

  render() {
    const { profile, auth } = this.props;

    return(
      <NavigationBarPresenter 
        title='GangsterHub'
        authenticated={!auth.isEmpty}
        isLoaded={auth.isLoaded}
        displayName={profile.displayName}
        photoURL={profile.avatarUrl}
        handleProfile={this.handleProfile}
        handleLogout={this.handleLogout}
        handleTitle={this.handleTitle}
        handleLogin={this.handleLogin}
        handleSignup={this.handleSignup}
      />
    );
  }

}

const mapStateToProps = state => ({
  // authenticated: getAuthenticated(state),
  // displayName: getDisplayName(state),
  // email: getEmail(state),
  uid: getFirebaseUID(state),
  // metadata: getMetadata(state),
  // photoURL: getPhotoURL(state),
  profile: getFirebaseProfile(state),
  auth: getFirebaseAuth(state),
});

const mapDispatchToProps = dispatch => ({
  onAuthLogout: uid => dispatch(doAuthLogout(uid)),
});

export default withRouter(
  withFirebase(connect(mapStateToProps, mapDispatchToProps)(NavigationBar))
);