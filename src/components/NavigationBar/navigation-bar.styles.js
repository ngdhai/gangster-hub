const styles = {
  root: {
    flexGrow: 1,
  },
  logo: {
    fontSize: 40,
    marginRight: 5,
  },
  grow: {
    flexGrow: 1,
    cursor: 'pointer',
    maxWidth: 'fit-content',
    marginRight: 'auto'
  },
  actionsGroup: {
  },
  userName: {
    display: 'inline',
  },
  
}

export default styles;