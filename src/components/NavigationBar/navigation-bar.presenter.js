import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { 
  AppBar, 
  Toolbar, 
  Typography,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Avatar
} from '@material-ui/core';
import {
  Face,
} from '@material-ui/icons';

import styles from './navigation-bar.styles';

class NavigationBarPresenter extends Component {
  state = {
    anchorEl: null
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget})
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleProfile = () => {
    const { handleProfile } = this.props;
    this.handleClose();
    handleProfile();
  }

  handleLogout = () => {
    const { handleLogout } = this.props;
    this.handleClose();
    handleLogout();
  }

  render() {
    const { isLoaded, classes, photoURL, authenticated, displayName, title, handleLogin, handleTitle, handleSignup } = this.props;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);
    return(
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Face className={classes.logo}/>
            <Typography align='center' variant="h4" color="inherit" className={classes.grow} onClick={handleTitle}>
              {
              title
              }
            </Typography>
            {
              authenticated ? 
              <div className={classes.actionsGroup}>
                <Typography variant="body2" color="inherit" className={classes.userName}>
                  {displayName}
                </Typography>
                <IconButton
                  aria-haspopup='true'
                  onClick={this.handleMenu}
                  color='inherit'
                >
                  <Avatar src={photoURL} alt='User Avatar'/>
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={this.handleClose}
                >
                  <MenuItem onClick={this.handleProfile}>Profile</MenuItem>
                  <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
                </Menu>
              </div>
              :
              isLoaded ?
              <div>
                <Button color='inherit' onClick={handleLogin}>Login</Button>
                <Button color='inherit' onClick={handleSignup}>Signup</Button>
              </div>
              :
              <div>Loading...</div>
            }
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(
  NavigationBarPresenter
);