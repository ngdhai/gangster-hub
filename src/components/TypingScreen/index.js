import React, { Component } from 'react';
import { connect } from 'react-redux';
import TypingScreenPresenter from './typing-screen.presenter';
import { uploadImage } from '../../imgur-api';
import { doChatSend } from '../../actions/chat';

class TypingScreen extends Component {
  handleOnSend = message => {
    const { user, currentUser, onSend } = this.props;
    if (message) {
      onSend(message, user, currentUser);
    }
  }

  handleUploadFile = async file => {
    const { user, currentUser, onSend } = this.props;
    const res = await uploadImage(file)
    const message = res.data.data.link;
    onSend(message, user, currentUser);
  }

  render() {
    return (
      <TypingScreenPresenter
        handleOnSend={this.handleOnSend}
        onUploadFile={this.handleUploadFile}
      />
    );
    
  }
}

const mapDispatchToProps = dispatch => ({
  onSend: (message, user, currentUser) => dispatch(doChatSend(message, user, currentUser))
});


export default connect(undefined, mapDispatchToProps)(TypingScreen);