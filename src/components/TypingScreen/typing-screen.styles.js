const styles = theme => ({
  typingScreen: {
    bottom: 0,
    order: 2,
    height: 165,
    backgroundColor: '#F2F5F8',
  },
  container: {
    padding: '0 20px 20px 20px'
  },
})

export default styles;