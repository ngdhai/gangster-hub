import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { 
  Paper,
  TextField,
  Grid,
  Button
} from '@material-ui/core';
import {
  AddPhotoAlternate
} from '@material-ui/icons';
import styles from './typing-screen.styles';


class TypingScreenPresenter extends Component {
  state = {
    message: ''
  }

  handleInputChange = event => {
    this.setState({
      message: event.target.value
    });
  }

  handleSubmit = () => {
    const { handleOnSend } = this.props;
    const { message } = this.state;
    this.setState({
      message: ''
    });
    handleOnSend(message);
  }

  handleFileInput = event => {
    const { onUploadFile } = this.props;
    const file = event.target.files[0];
    onUploadFile(file);
  }

  render() {
    const { classes } = this.props;
    const { message } = this.state;
    return(
      <Paper className={classes.typingScreen}>
        <Grid container justify='center' className={classes.container}>
          <TextField
            id="outlined-multiline-static"
            label="Message"
            multiline
            rows="3"
            fullWidth
            margin="normal"
            variant="outlined"
            value={message}
            onChange={this.handleInputChange}
          />
          <Grid item xs={6}>
            <Grid container justify='flex-start'>
              {/* <Button color="primary" className={classes.button}>
                  <AttachFile/>
              </Button>
              <Button color="primary" className={classes.button}>
                  <AddPhotoAlternate component={Test}/>
              </Button> */}
              <input
                accept="image/*"
                hidden
                id="raised-button-file"
                type="file"
                onChange={this.handleFileInput}
              />
              <label htmlFor="raised-button-file">
                <Button variant="contained" component="span">
                  <AddPhotoAlternate/>
                </Button>
              </label> 
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Grid container justify='flex-end'>
              <Button color="secondary" className={classes.button} onClick={this.handleSubmit}>
                  Send
              </Button>
            </Grid>
          </Grid>
        </Grid>
        
      </Paper>
    );
  }
}

export default withStyles(styles)( 
  TypingScreenPresenter
);
