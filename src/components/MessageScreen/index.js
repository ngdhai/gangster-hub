import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageScreenPresenter from './message-screen.presenter';
import { getChatLog } from '../../selectors/chat.selectors';

class MessageScreen extends Component {
  render() {
    const { chatLog, currentUser, user } = this.props;
    return <MessageScreenPresenter currentUser={currentUser} user={user} chatLog={chatLog || []}/>
  }
}

const mapStateToProps = state => ({
  chatLog: getChatLog(state)
});

export default connect(mapStateToProps)(MessageScreen);