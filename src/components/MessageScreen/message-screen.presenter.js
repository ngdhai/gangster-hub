import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Paper, 
} from '@material-ui/core';
import {
  OwnMessage,
  OpMessage
} from '..';
import styles from './message-screen.styles';

export default withStyles(styles)(({classes, currentUser, user, chatLog}) => 
  <Paper className={classes.messageScreen} elevation={0}>
    {
      chatLog.sort((a, b) => a.timeStamp - b.timeStamp).map(chat => 
        chat.uid === currentUser.uid ?
        <OwnMessage key={chat.timeStamp} chat={chat} user={currentUser}/> 
        : 
        <OpMessage key={chat.timeStamp} chat={chat} user={user}/>
      )
    }
  </Paper>
);