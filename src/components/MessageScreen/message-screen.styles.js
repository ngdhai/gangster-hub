import { green, grey, indigo } from '@material-ui/core/colors';

const styles = theme => ({
  messageScreen: {
    flex: 1,
    order: 1,
    overflowY: 'auto',
    // overflowX: 'hidden',
    minHeight: '0px',
    maxHeight: '60vh',
    flexDirection: 'column',
    padding: '10px 20px 20px 20px'
  },
  iconOnline: {
    width: 20,
    height: 20,
    color: green[800]
  },
  iconOffline:  {
    width: 20,
    height: 20,
    color: grey[500]
  },
  text: {
    padding: 10,
    backgroundColor: green[500]
  },

  text2: {
    padding: 10,
    backgroundColor: indigo[200]
  },
  ownMessage: {
    marginBottom: 20
  },
  opMessage: {
    marginBottom: 20
  }
 
});

export default styles;