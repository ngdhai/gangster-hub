import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withFirebase } from 'react-redux-firebase';
import { 
  // getAuthenticated,
  getFirebaseAuth
} from '../../selectors/auth.selectors';
import ChatBoxPresenter from './chat-box.presenter';

class ChatBox extends Component {
  render() {
    const { auth } = this.props;
    return (
      !auth.isEmpty ? <ChatBoxPresenter/> : null
    );
  }

  componentDidMount() {
    const { auth } = this.props;
    setTimeout(() => {
      if (auth.isEmpty) {
        this.props.history.push('/login');
      }
    }, 1500);
  }
}

const mapStateToProps = state => ({
  auth: getFirebaseAuth(state)
})

export default withRouter(
  withFirebase(connect(mapStateToProps)(ChatBox))
);