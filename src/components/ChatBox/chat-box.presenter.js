import React from 'react';
import { Grid, Paper } from '@material-ui/core';
import { UserPane, ChatPane } from '..';
import { withStyles } from '@material-ui/core/styles';
import styles from './chat-box.styles';


export default withStyles(styles)(({classes}) =>
  <Grid container justify='center' className={classes.root}>
    <Paper className={classes.paper} elevation={24}>
      <Grid container spacing={0} className={classes.appContainer}>
        <Grid item xs={4} className={classes.userPane}>
          <UserPane/>
        </Grid>
        <Grid item xs={8}>
          <ChatPane/>
        </Grid>
      </Grid>
    </Paper>
  </Grid>
);
