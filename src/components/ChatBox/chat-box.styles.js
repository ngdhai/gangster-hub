const styles = {
  root: {
    marginTop: 20,
    height: '95%',
    minHeight: 400
  },
  paper: {
    width: '50%',
    minWidth: 400,
  },
  appContainer: {
    height: '100%'
  },
  userPane: {
    backgroundColor: '#464953',
    height: '100%',
    borderRadius: '5px 0 0 5px',
    padding: '15px 30px 30px 15px'
  }
}

export default styles;