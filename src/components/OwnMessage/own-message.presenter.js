import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Paper, 
  Typography,
  Grid,
} from '@material-ui/core';
import {
  AccountCircle
} from '@material-ui/icons';
import {
  InChatImage
} from '..';
import styles from './own-message.styles.js';

export default withStyles(styles)(({classes, chat, user}) => 
  <Grid container justify='flex-start' spacing={8} className={classes.ownMessage}>
    <Grid container>
      <Grid item xs={1}>
        <Grid container justify='center'>
          <AccountCircle className={user.online ? classes.iconOnline: classes.iconOffline}/>
        </Grid>
      </Grid>
      <Grid item xs={11}>
        <Typography variant='body2'>
          {user.displayName}
        </Typography>
      </Grid>
    </Grid>
    <Grid container>
      <Grid item xs={10}>
        <Paper className={classes.text}>
          <Typography>{chat.message}</Typography>
          {
            /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/g.test(chat.message) ? 
            <InChatImage message={chat.message}/> : null
          }
        </Paper>
      </Grid>
    </Grid>
  </Grid>
);
