import React, { Component } from 'react';
import OwnMessagePresenter from './own-message.presenter';

class OwnMessage extends Component {
  render() {
    const { chat, user } = this.props;
    return <OwnMessagePresenter chat={chat} user={user}/>
  }
}

export default OwnMessage;