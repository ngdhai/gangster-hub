import { green, grey, indigo } from '@material-ui/core/colors';

const styles = theme => ({
  iconOnline: {
    width: 20,
    height: 20,
    color: green[800]
  },
  iconOffline: {
    width: 20,
    height: 20,
    color: grey[500]
  },
  text: {
    padding: 10,
    backgroundColor: green[500],
    wordBreak: 'break-word'

  },
  text2: {
    padding: 10,
    backgroundColor: indigo[200]
  },
  ownMessage: {
    marginBottom: 20
  },
  opMessage: {
    marginBottom: 20
  },
});

export default styles;