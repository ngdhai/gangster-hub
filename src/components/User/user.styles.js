import { lightGreen } from '@material-ui/core/colors';
import { createMuiTheme } from '@material-ui/core/styles';

export const theme = createMuiTheme({
  overrides: {
    MuiListItem: {
      divider: {
        borderBottomColor: lightGreen[400]
      }
    },
    MuiTypography: {
      colorTextPrimary: {
        color: lightGreen[200]
      },
      colorTextSecondary: {
        color: lightGreen[50]
      }
    }
  }
})

const styles = theme => ({
  userAvatar: {
    border: '2px solid',
    borderColor: '#63656E'
  },
  listItem: {
    paddingLeft: 8,
  },
  listItemTextTypography: {
    color: lightGreen[50]
  }
});

export default styles;