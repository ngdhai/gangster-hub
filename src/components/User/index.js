import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserPresenter from './user.presenter';
import { doUserSelect } from '../../actions/user';
import { doChatStop } from '../../actions/chat';
import { 
  // getCurrentUser,
  getFirebaseCurrentUser 
} from '../../selectors/auth.selectors';

class User extends Component {
  handleUserClick = () => {
    const { onUserSelected, user: {uid}, currentUser, onChatStop } = this.props;
    onChatStop(currentUser.uid, uid);
    onUserSelected(uid, currentUser.uid);
  }

 render() {
   const { user } = this.props;
   return <UserPresenter user={user} handleUserClick={this.handleUserClick}/>
 }
}

const mapDispatchToProps = dispatch => ({
  onUserSelected: (uid, curUID) => dispatch(doUserSelect(uid, curUID)),
  onChatStop : (curUID, uid) => dispatch(doChatStop(curUID, uid))
});

const mapStateToProps = state => ({
  currentUser: getFirebaseCurrentUser(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(User);