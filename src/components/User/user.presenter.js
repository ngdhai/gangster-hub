import React, { Component } from 'react';
import { 
  ListItem,
  Avatar,
  ListItemText,
} from '@material-ui/core';
import { withStyles, MuiThemeProvider } from '@material-ui/core/styles';
import * as moment from 'moment';
import styles, { theme } from './user.styles';

class UserPresenter extends Component {
  render() {
    const { classes, user, handleUserClick } = this.props;
    const { selected, displayName, /*metadata,*/ avatarUrl, logoutTime, online } = user;
    return (
      <>
        <MuiThemeProvider theme={theme}>
          <ListItem 
            button 
            disableGutters
            className={classes.listItem} 
            onClick={handleUserClick} 
            divider={selected}
          >
            <Avatar 
              alt='User Avatar' 
              src={avatarUrl}
              className={classes.userAvatar}
            />
            <ListItemText 
              primary={displayName} 
              secondary={online ?  'Online' : `Offline ${moment(logoutTime).fromNow()}`}
              primaryTypographyProps={{
                variant: 'body2',
                noWrap: true,
                color: 'textPrimary'
              }}
              secondaryTypographyProps={{
                variant: 'caption',
                noWrap: true,
                color: 'textSecondary'
              }}
            />
          </ListItem>
        </MuiThemeProvider>
      </>
    );
  }
}

export default withStyles(styles)(UserPresenter);