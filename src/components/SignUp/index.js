import React, { Component } from 'react';
import SignUpPresenter from './signup.presenter';

class SignUp extends Component {
  render() {
    return <SignUpPresenter/>;
  }
}

export default SignUp;