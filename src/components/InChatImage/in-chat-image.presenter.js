import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardMedia
} from '@material-ui/core';
import styles from './in-chat-image.styles';


export default withStyles(styles)(({classes, images}) => {
  return (
    <Card>
      <CardActionArea>
        {
          images.map((image, i) =>
            <CardMedia
              key={i}
              className={classes.media}
              image={image}
              title={image}
            />
          )
        }
       
      </CardActionArea>
    </Card>
  );
}); 
