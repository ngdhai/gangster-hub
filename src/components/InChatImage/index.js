import React, { Component } from 'react';
import InChatImagePresenter from './in-chat-image.presenter';

class InChatImage extends Component {
  render() {
    const { message } = this.props;
    console.log(message);
    const images = message.match(/(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/g);
    return <InChatImagePresenter images={images}/>
  }
}

export default InChatImage;