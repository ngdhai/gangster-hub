import React from 'react';
import { List } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { User } from '..';
import styles from './user-list.styles';

export default withStyles(styles)(({classes, users}) => 
  <List>
    {
      users.map((user, i) =>
        <User 
          key={i} 
          user={user}
        />
      )
    }
  </List>
);
