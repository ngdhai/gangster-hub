import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withFirebase } from 'react-redux-firebase';
import UserListPresenter from './user-list.presenter';
import { /*getCurrentUser,*/ /*getFirebaseCurrentUser,*/ getFirebaseCurrentUserProfile } from '../../selectors/auth.selectors';
import { getUsersBaseOnFilter } from '../../selectors/user.selectors';


class UserList extends Component {
  sort = (users, currentUser) => {
    const currentFitlerd = users.filter(user => user.uid !== currentUser.uid);
    const lastChatSort = currentFitlerd.sort((a, b) => {
      const x = currentUser.lastChat && currentUser.lastChat[a.uid];
      const y = currentUser.lastChat && currentUser.lastChat[b.uid];
      if (x && y) {
        return y - x;
      } else if (x) {
        return -1;
      } else if (y) {
        return 1;
      }
      return 0;
    });
    const starUserSorted = lastChatSort.sort((a, b) => {
      const aStared = currentUser.starUser ? Object.keys(currentUser.starUser).includes(a.uid) : false;
      const bStared = currentUser.starUser ? Object.keys(currentUser.starUser).includes(b.uid) : false;
      return bStared - aStared;
    });
    const onlineSorted = starUserSorted.sort((a, b) => {
      return b.online - a.online;
    });
   
    return onlineSorted.sort();
  }
  render() {
    const { users, currentUser } = this.props;
    return (
      <UserListPresenter 
        users={this.sort(users, currentUser)}
      />
    )
  }

}

const mapStateToProps = (state, props) => ({
    users: getUsersBaseOnFilter(state, props),
    currentUser: getFirebaseCurrentUserProfile(state),

});



export default withFirebase(connect(mapStateToProps)(UserList));