import { lightGreen } from '@material-ui/core/colors';

const styles = theme => ({
  root: {
    backgroundColor: '#464953',
    height: '100%',
    borderRadius: '5px 0 0 5px',
    padding: '15px 15px 30px 15px'
  },
  redColor: {
    color: "white"
  },
  searchBoxDivider: {
    marginTop: 30,
    backgroundColor: lightGreen[300],
    height: 3
  }
})

export default styles;