import React from 'react';
import { 
  Divider
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import styles from './user-pane.styles';
import { 
  SearchBox,
  UserList
} from '..';


export default withStyles(styles)(({classes, onFilterChange, userFilter}) => 
  <>
    <SearchBox onChange={onFilterChange}/>
    <Divider className={classes.searchBoxDivider}/>
    <UserList userFilter={userFilter}></UserList>
  </>
);