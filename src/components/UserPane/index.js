import React, { Component } from 'react';
import UserPanePresenter from './user-pane.presenter';

class UserPane extends Component {
  state = {
    userFilter: ''
  }

  handleFilterChange = value => {
    this.setState({
      userFilter: value
    });
  }

  render() {
    const { userFilter } = this.state;
    return <UserPanePresenter 
              onFilterChange={this.handleFilterChange}
              userFilter={userFilter}
            />
  }

}

export default UserPane;