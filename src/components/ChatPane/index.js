import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSelectedUser } from '../../selectors/user.selectors';
import ChatPanePresenter from './chat-pane.presenter';
import { 
  // getCurrentUser,
  getFirebaseCurrentUserProfile
} from '../../selectors/auth.selectors';

class ChatPane extends Component {
  render() {
    const { user, currentUser } = this.props;
    return user ? <ChatPanePresenter user={user} currentUser={currentUser}/> : null;
  }
}

const mapStateToProps = state => ({
  user: getSelectedUser(state),
  currentUser: getFirebaseCurrentUserProfile(state)
});

export default connect(mapStateToProps)(ChatPane);
