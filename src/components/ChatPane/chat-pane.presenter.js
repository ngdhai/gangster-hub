import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import styles from './chat-pane.styles';
import { TypingScreen, MessageScreen, StatusBar } from '..';

export default withStyles(styles)(({classes, user, currentUser}) => {
  return (
    <div className={classes.root}>
      <StatusBar currentUser={currentUser} user={user}/>
      <MessageScreen currentUser={currentUser} user={user}/>
      <TypingScreen currentUser={currentUser} user={user}/>
    </div>
  );
});
