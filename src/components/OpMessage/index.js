import React, { Component } from 'react';
import OpMessagePresenter from './op-message.presenter';

class OpMessage extends Component {
  render() {
    const { chat, user } = this.props;
    return <OpMessagePresenter chat={chat} user={user}/>
  }
}

export default OpMessage;