import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
  Paper, 
  Typography,
  Grid
} from '@material-ui/core';
import {
  AccountCircle
} from '@material-ui/icons';
import styles from './op-message.styles.js';
import { InChatImage } from '..';

export default withStyles(styles)(({classes, user, chat}) => 
  <Grid container justify='flex-end' spacing={8} className={classes.opMessage}>
    <Grid container>
      <Grid item xs={11}>
        <Grid container justify='flex-end'>
          <Typography variant='body2'>
            {user.displayName}
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={1}>
        <Grid container justify='center'>
          <AccountCircle className={user.online ?  classes.iconOnline: classes.iconOffline}/>
        </Grid>
      </Grid>
    </Grid>
    <Grid container justify='flex-end'>
      <Grid item xs={10}>
        <Paper className={classes.text}>
          <Typography>{chat.message}</Typography>
          {
            /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)/g.test(chat.message) ? 
            <InChatImage message={chat.message}/> : null
          }
        </Paper>
      </Grid>
    </Grid>
  </Grid>
);
