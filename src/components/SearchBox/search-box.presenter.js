import React from 'react';
import  { TextField, MuiThemeProvider } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import styles, { theme } from './search-box.styles';

export default withStyles(styles)(({classes, onChange}) =>
  <MuiThemeProvider theme={theme}>
    <TextField
      label="Search"
      id="user-filter"
      variant='outlined'
      fullWidth
      InputProps={{
        className: classes.input,
      }}
      InputLabelProps={{
        FormLabelClasses: {
          root: classes.labelColor,
        }
      }}
      onChange={onChange}
    />
  </MuiThemeProvider>
)