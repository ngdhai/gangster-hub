import React, { Component } from 'react';
import SearchBoxPresenter from './search-box.presenter';

class SearchBox extends Component {
  handleOnChange = event => {
    const { onChange } = this.props;
    onChange(event.target.value);
  }
  render() {
    return <SearchBoxPresenter onChange={this.handleOnChange}/>
  }
}

export default SearchBox;