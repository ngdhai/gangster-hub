import { createMuiTheme } from '@material-ui/core/styles';
import {  lightGreen } from '@material-ui/core/colors';

export const theme = createMuiTheme({
  overrides: {
    MuiOutlinedInput: {
      root: {
        '&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
          borderColor: lightGreen[100]
        },
      },
      notchedOutline: {
        borderColor: lightGreen[200],
      }
    }
  },
  palette: {
    primary: lightGreen
  },
  typography: { useNextVariants: true },
});

const styles = theme => ({
  input: {
    color: lightGreen[50],
  },
  labelColor: {
    color: lightGreen[50],
  },
});

export default styles;