import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withFirebase } from 'react-redux-firebase';
import { doAuthLoginWithGoogle } from '../../actions/auth';
import LoginPresenter from './login.presenter';
import { 
  // getAuthenticated, 
  getFirebaseAuth, 
} from '../../selectors/auth.selectors';

class Login extends Component {
  handleSubmit = async (email, password) => {
    const { history, firebase } = this.props;
    if (await firebase.login({
      email, 
      password
    })) {
      history.push('/');
    }
  }

  handleGoogleLogin = async () => {
    const { history, firebase } = this.props;
    if (await firebase.login({
      provider: 'google',
      type: 'popup'
    })) {
      history.push('/');
    }
  }

  render() {
    const { auth, history } = this.props;
    return (
      auth.isLoaded ?
        auth.isEmpty ?
          <LoginPresenter 
            handleSubmit={this.handleSubmit}
            handleGoogleLogin={this.handleGoogleLogin}
          />
          :
          <>
            {
              history.push('/')
            }
          </> 
        :
        <div>Loading...</div>
        
    );
  }

}

const mapStateToProps = state => ({
  // authenticated: getAuthenticated(state),
  auth: getFirebaseAuth(state),
});

const mapDispatchToProps = dispatch => ({
  onLoginWithGoogle: (history, redirect) => dispatch(doAuthLoginWithGoogle(history, redirect))
});

export default withRouter(
  withFirebase(connect(mapStateToProps, mapDispatchToProps)(Login))
);