import React, { Component } from 'react';
import {
  Paper,
  Typography,
  Grid,
  TextField,
  Button,
  Divider
} from '@material-ui/core';
import {
  LockOutlined
} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import styles from './login.styles';

class LoginPresenter extends Component {
  state = {
    email: '',
    password: '',
    isvalidated: false
  }

  handleEmailChange = event => {
    this.setState({ email: event.target.value });
  }

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    const { handleSubmit } = this.props;
    const { email, password } = this.state;
    this.setState({ email: '', password: '' });
    handleSubmit(email, password);
  }

  // Form validate

  render() {
    const { classes, handleGoogleLogin } = this.props;
    const { email, password } = this.state;
    return(
      <form onSubmit={this.handleSubmit}>
        <Grid container justify='center'>
            <Paper className={classes.paper} elevation={8}>
              <Grid container justify='center'>
                <LockOutlined fontSize='large' className={classes.lockIcon}/>
              </Grid>
              <Typography variant='headline' align='center'>
                Login
              </Typography>
              <Grid item sm={12}>
                <TextField 
                  id="email-input"
                  label="Email Address"
                  className={classes.textField}
                  type="email"
                  fullWidth
                  required
                  value={email}
                  margin="normal"
                  onChange={this.handleEmailChange}
                />
                <TextField 
                  id="password-input"
                  label="Password"
                  className={classes.textField}
                  type="password"
                  fullWidth
                  required
                  value={password}
                  margin="normal"
                  onChange={this.handlePasswordChange}
                />
                <Button 
                  variant="outlined" 
                  className={classes.button} 
                  color="primary"
                  type='submit'
                  fullWidth
                  onClick={this.handleSubmit}
                  >
                  login
                </Button>
              </Grid>
            </Paper>
            <Grid container justify='center' className={classes.redirect}>
              <Typography align='center' variant='body2'>
                Don't have account? &nbsp; <Link to='/signup'>Signup</Link>
                
              </Typography>
              
            </Grid>
            <Paper className={classes.subPaper} elevation={8}>
              <Typography variant='body2'>
                Or login with
              </Typography>
              <Divider />
              <Button 
                  variant="outlined" 
                  className={classes.googleBtn} 
                  color="secondary"
                  type='button'
                  fullWidth
                  onClick={handleGoogleLogin}
                  >
                  <img src='assets/google.png' alt='google-icon' style={{width: '24px', height: '24px'}}/> &nbsp; Google
                </Button>
            </Paper>
        </Grid>
      </form>
      
    );
  }
}

export default withStyles(styles)(LoginPresenter);