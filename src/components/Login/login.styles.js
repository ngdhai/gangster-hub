const styles = theme => ({
  paper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    width: 340
  },
  lockIcon: {
    marginBottom: 10,
    backgroundColor: '#7986CB',
    borderRadius: '50%',
    padding: 10,
    color: '#fff',
  },
  textField: {
    marginBottom: 10,
  },
  button: {
    marginTop: 35
  },
  redirect: {
    marginTop: 25
  },
  subPaper: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    width: 250,
    marginTop: 15
  },
  googleBtn: {
    marginTop: 10
  }
});

export default styles;