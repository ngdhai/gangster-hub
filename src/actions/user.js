import {
  USER_SELECT,
  USER_ADD,
  USER_FETCH_SIGNIN,
  USER_STAR,
  USER_UNSTAR,
} from './types';

const doUserSelect = (uid, currentUserUID) => ({
  type: USER_SELECT,
  payload: {
    uid,
    curUID: currentUserUID
  }
});

const doUserAdd = users => ({
  type: USER_ADD,
  payload: {
    users
  }
});

const doUserFetchSignIn = databaseRef => ({
  type: USER_FETCH_SIGNIN,
  payload: {
    databaseRef
  }
});

const doUserStar = (currentUser, user) => ({
  type: USER_STAR,
  payload: {
    currentUser,
    user
  }
});

const doUserUnStar = (currentUser, user) => ({
  type: USER_UNSTAR,
  payload: {
    currentUser,
    user
  }
});


export {
  doUserSelect,
  doUserAdd,
  doUserFetchSignIn,
  doUserStar,
  doUserUnStar
}