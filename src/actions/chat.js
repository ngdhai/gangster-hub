import {
  CHAT_SEND,
  CHAT_STOP,
  // CHAT_TYPING,
  // CHAT_WATCH,
  // CHAT_CREATECHATLOG,
  CHAT_CHATLOGADD
} from './types';

const doChatSend = (message, user, currentUser) => ({
  type: CHAT_SEND,
  payload: {
    message,
    user,
    currentUser
  }
});

const doChatLogAdd = chatLog => ({
  type: CHAT_CHATLOGADD,
  payload: {
    chatLog
  }
});

const doChatStop = (curUID, uid) => ({
  type: CHAT_STOP,
  payload: {
    curUID,
    uid
  }
});

export {
  doChatSend,
  doChatLogAdd,
  doChatStop
}