export const AUTH_AUTHENTICATED = 'AUTH_AUTHENTICATED';
export const AUTH_UNAUTHENTICATED = 'AUTH_UNAUTHENTICATED';
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_WITH_GOOGLE = 'AUTH_LOGIN_WITH_GOOGLE';
export const AUTH_LOGOUT = 'AUTH_LOGNOUT';
export const AUTH_FETCH_USER = 'AUTH_FETCH_USER';

export const USER_SELECT = 'USER_SELECT';
export const USER_ADD = 'USER_ADD';
export const USER_FETCH_SIGNIN = 'USER_FETCH_SIGNIN';
export const USER_SETONLINE = 'USER_SETONLINE';
export const USER_SETOFFLINE = 'USER_SETOFFLINE';
export const USER_STAR = 'USER_STAR';
export const USER_UNSTAR = 'USER_UNSTAR';

export const CHAT_WATCH = 'CHAT_WATCH'; //Craete channel hand ehandle
export const CHAT_STOP = 'CHAT_STOP'; // Stop channel
export const CHAT_TYPING = 'CHAT_TYPING';
export const CHAT_SEND = 'CHAT_SEND';
export const CHAT_TEST = 'CHAT_TEST';
export const CHAT_CHATLOGADD = 'CHAT_CHATLOGADD';

export const CANCEL = 'CANCEL';