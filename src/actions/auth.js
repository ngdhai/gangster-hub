import { 
  AUTH_AUTHENTICATED,
  AUTH_UNAUTHENTICATED,
  AUTH_LOGIN,
  AUTH_LOGOUT,
  AUTH_LOGIN_WITH_GOOGLE,
  AUTH_FETCH_USER,
} from './types';

const doAuthAuthenticated = user => ({
  type: AUTH_AUTHENTICATED,
  payload: {
    authenticated: true,
    displayName: user.displayName,
    email: user.email,
    uid: user.uid,
    metadata: user.metadata,
    photoURL: user.photoURL,
  }
});

const doAuthUnAuthenticated = () => ({
  type: AUTH_UNAUTHENTICATED,
  payload: {
    authenticated: false,
  }
});

const doAuthLogin = () => ({
  type: AUTH_LOGIN
});

const doAuthLoginWithGoogle = (history, redirect) => ({
  type: AUTH_LOGIN_WITH_GOOGLE,
  history,
  redirect,
});

const doAuthLogout =  uid => ({
  type: AUTH_LOGOUT,
  payload: {
    uid
  }
});

const doAuthFetchUser = authRef => ({
  type: AUTH_FETCH_USER,
  payload: {
    authRef
  }
});

export {
  doAuthAuthenticated,
  doAuthUnAuthenticated,
  doAuthLogin,
  doAuthLoginWithGoogle,
  doAuthLogout,
  doAuthFetchUser,
}