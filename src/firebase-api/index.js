import { auth, database } from 'firebase';
import { googleProvider } from './provider';

const signinWithPopup = () => {
  return auth().signInWithPopup(googleProvider);
};

const updateLogoutTime = async uid => {
  await database().ref(`users/${uid}`).update({
    logoutTime: Date.now()
  });
};


const updateLoggingStatus = async uid => {
  await database().ref(`users/${uid}`).update({
    logoutTime: null
  });
};

const logoutWithGoogle = () => {
  return auth().signOut();
};

const addUserToDatabase = async user => {
  await database().ref(`users/${user.uid}`).set({
    displayName: user.displayName,
    email: user.email,
    metadata: user.metadata,
    photoURL: user.photoURL,
    logoutTime: null
  });
};

const getChatLog = async (currentUserUID, userUID) => {
  const snapshot = await database().ref(`users/${currentUserUID}/chatLog/${userUID}`).once('value');
  return snapshot.val();
};

const createChatLog = async (message, userUID, currentUserUID) => {
  const newChatLogKey = database().ref('chatLog').push().key;
  await database().ref(`users/${currentUserUID}/chatLog`).update({
    [userUID]: newChatLogKey
  });

  await database().ref(`users/${userUID}/chatLog`).update({
    [currentUserUID]: newChatLogKey
  });

  // await database().ref(`chatLog/${newChatLogKey}`).set({
  //   [currentUserUID]: {
  //     [Date.now()]: message
  //   }
  // });

  return newChatLogKey;
};

const updateChatLog = async (currentUser, message, chatLogId, user) => {
  await database().ref(`chatLog/${chatLogId}/${currentUser.uid}`).update({
    [Date.now()]: message
  });
  await database().ref(`users/${currentUser.uid}/lastChat`).update({
    [user.uid]: Date.now()
  });
};

const updateStarUser = async (currentUser, user) => {
  await database().ref(`users/${currentUser.uid}/starUser`).update({
    [user.uid]: true
  });
};

const updateUnStarUser = async (currentUser, user) => {
  await database().ref(`users/${currentUser.uid}/starUser`).update({
    [user.uid]: null
  });
};

export {
  signinWithPopup,
  logoutWithGoogle,
  addUserToDatabase,
  updateLogoutTime,
  updateLoggingStatus,
  createChatLog,
  getChatLog,
  updateChatLog,
  updateStarUser,
  updateUnStarUser
}