import { auth } from 'firebase';

const googleProvider = new auth.GoogleAuthProvider();

export {
  googleProvider
}