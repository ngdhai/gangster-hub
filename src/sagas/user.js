import { put, take, call, cancelled, race } from 'redux-saga/effects';
import { createUsersChannel, createChatLogChannel } from './event-chanel';
import { doUserAdd } from '../actions/user';
import { doChatLogAdd } from '../actions/chat';
import { getChatLog, createChatLog, updateStarUser, updateUnStarUser } from '../firebase-api';
import { database } from 'firebase';
import { CHAT_STOP } from '../actions/types';


function *handleUserFetchSignIn(action) {
  const { databaseRef } = action.payload;
  const channel = createUsersChannel(databaseRef);
  while(true) {
    try {
      const snapshot = yield take(channel);
      const users = Object.keys(snapshot).map(key => ({
        uid: key,
        ...snapshot[key],
      }));
      yield put(doUserAdd(users))
    } catch(error) {
      console.log(error);
    }
  }
};

function *handleUserSelect(action) {
  // get current user chat log id
  const { curUID, uid } = action.payload;
  let chatLogId = yield call(getChatLog, curUID, uid);
  if (!chatLogId) {
    chatLogId = yield call(createChatLog, null, uid, curUID);
  }
  const channel = yield call(createChatLogChannel, database(), chatLogId);
  try {
  while(true) {
      const { stop, value } = yield race({
        stop: take(CHAT_STOP),
        value: take(channel)
      })
      if (stop) {
         // hook event to stop channel
        channel.close();
      } else if (value){
        yield put(doChatLogAdd(value));
        // put action to update state
      }
    }
  } finally {
    if (yield cancelled()) {
      console.log('cancelled')
    }
  }
 
};

function *handleUserStar(action) {
  // update firebase database
  const { currentUser, user } = action.payload;
  yield updateStarUser(currentUser, user);
};

function *handleUserUnStar(action) {
  const { currentUser, user } = action.payload;
  yield updateUnStarUser(currentUser, user);
}

export {
  handleUserFetchSignIn,
  handleUserSelect,
  handleUserStar,
  handleUserUnStar
}