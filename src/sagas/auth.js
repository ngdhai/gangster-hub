import { call, put, take } from 'redux-saga/effects';
import { 
  // doAuthAuthenticated, 
  doAuthUnAuthenticated 
} from '../actions/auth';
import { 
  signinWithPopup, 
  logoutWithGoogle, 
  addUserToDatabase, 
  updateLogoutTime, 
  // updateLoggingStatus 
} from '../firebase-api';
import { 
  // createAuthChannel, 
  createPresenceChannel 
} from './event-chanel';
import { database } from 'firebase';

function *handleUserLoginWithGoogle(action) {
  const { history, redirect } = action;
  try {
    const result = yield call(signinWithPopup);
    if (result.additionalUserInfo.isNewUser) {
      yield call(addUserToDatabase, result.user);
    }
    yield call(history.push, redirect);
  } catch (error) {
    console.log(error);
  }
};

function *handleLogout(action) {
  const { uid } = action.payload;
  try {
    yield call(logoutWithGoogle);
    yield call(updateLogoutTime, uid);
    yield put(doAuthUnAuthenticated());
  } catch(error) {
    console.log(error);
  }
}


// function *handleFetchUser(action) {
//   const { authRef } = action.payload;
//   const channel = createAuthChannel(authRef);

//   while(true) {
//     try {
//       const user = yield take(channel);
//       if (user) {
//         yield call(updateLoggingStatus, user.uid);
//         yield put(doAuthAuthenticated(user));
//       }
//     } catch (error) {
//       console.log(error)
//     }
//   }

// };

function *handleFetchUser(action) {
  const channel = createPresenceChannel(database());

  while(true) {
    try {
      const presences = yield take(channel);
      const usersSnapshot = yield database().ref('users').once('value');
      const users = Object.keys(usersSnapshot.val());
      if (presences) {
        users.forEach(uid => {
          presences.includes(uid) ?  
            database().ref(`users/${uid}`).update({
              online: true
            })
            :
            database().ref(`users/${uid}`).update({
              online: false
            })
        });
      }
    } catch (error) {
      console.log(error);
    }
  } 
}


export {
  handleUserLoginWithGoogle,
  handleFetchUser,
  handleLogout
}