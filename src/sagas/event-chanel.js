import { eventChannel } from 'redux-saga';

const createAuthChannel = authRef => {
  const listener = eventChannel(emit => {
    authRef.onAuthStateChanged(user => {
      if (user) {
        emit(user);
      }
    });
    return () => {
      console.log('unsubscribe auth chanel');
    }
  });

  return listener;
};

const createUsersChannel = databaseRef => {
  const listener = eventChannel(emit => {
    databaseRef.ref('users').on('value', snapshot => {
        emit(snapshot.val());
    });
    return () => {
      databaseRef.off();
    }
  });
  return listener;
};

const createChatLogChannel = (databaseRef, chatLogId) => {
  const listener = eventChannel(emit => {
    databaseRef.ref(`chatLog/${chatLogId}`).on('value', snapshot => {
      emit(snapshot.val() || {});
    });
    return () => {
      databaseRef.ref(`chatLog/${chatLogId}`).off('value');
    }
  });
  return listener;
};

const createPresenceChannel = databaseRef => {
  const listener = eventChannel(emit => {
    databaseRef.ref('presence').on('value', snapshot => {
      
      emit(snapshot.val() ? Object.keys(snapshot.val()) : []);
    });
    return () => {
      databaseRef.ref('presence').off('value');
    }
  });
  return listener;
}

export{
  createAuthChannel,
  createUsersChannel,
  createChatLogChannel,
  createPresenceChannel
} 