import { call } from 'redux-saga/effects';
import { createChatLog, getChatLog, updateChatLog } from '../firebase-api';

function * handleChatSend(action) {
  const { currentUser, user, message } = action.payload;
  const currentUserChatLog = yield call(getChatLog, currentUser.uid, user.uid);
  if (currentUserChatLog) {
    yield call(updateChatLog, currentUser, message, currentUserChatLog, user);
    // update chat log
  } else {
    const chatLogId = yield call(createChatLog, message, user, currentUser);
    console.log(`LISTEN TO CHATLOG ${chatLogId}`);
  }
  // yield call(createChatLog, message, user, currentUser);
}

export { 
  handleChatSend
}
