import { takeEvery, all } from 'redux-saga/effects';
import { 
  AUTH_LOGIN_WITH_GOOGLE, 
  AUTH_FETCH_USER, 
  AUTH_LOGOUT,
  USER_FETCH_SIGNIN,
  USER_SELECT,
  CHAT_SEND,
  USER_STAR,
  USER_UNSTAR
} from '../actions/types';
import { 
  handleUserLoginWithGoogle, 
  handleFetchUser, 
  handleLogout
} from './auth';
import {
  handleUserFetchSignIn,
  handleUserSelect,
  handleUserStar,
  handleUserUnStar
} from './user';
import { handleChatSend } from './chat';

function *rootSaga() {
  yield all([
    takeEvery(AUTH_LOGIN_WITH_GOOGLE, handleUserLoginWithGoogle),
    takeEvery(USER_FETCH_SIGNIN, handleUserFetchSignIn),
    takeEvery(AUTH_FETCH_USER, handleFetchUser),
    takeEvery(AUTH_LOGOUT, handleLogout),
    takeEvery(CHAT_SEND, handleChatSend),
    takeEvery(USER_SELECT, handleUserSelect),
    takeEvery(USER_STAR, handleUserStar),
    takeEvery(USER_UNSTAR, handleUserUnStar)
  ]);
};

export default rootSaga;