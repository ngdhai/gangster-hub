import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import { reactReduxFirebase } from 'react-redux-firebase';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers';
import rootSaga from '../sagas';
import firebase from 'firebase';
import config from '../firebase-api/config';
firebase.initializeApp(config);

const logger = createLogger();
const saga = createSagaMiddleware();

const rrfConfig = {
  userProfile: 'users',
  presence: 'presence'
};
const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig)
)(createStore);

const store = createStoreWithFirebase(
  rootReducer,
  undefined,
  applyMiddleware(saga, logger)
);

saga.run(rootSaga);

export default store;
