import axios from 'axios';

const apiUrl = 'https://api.imgur.com/3/image';
const apiKey = '27b6c639e67effc';

const uploadImage =  file => {
  const formData = new FormData();
  formData.append('image', file);
  const res = axios.post(
    apiUrl,
    formData,
    {
    headers: {
      Authorization: `Client-ID ${apiKey}`,
      'content-type': 'multipart/form-data'
    }
  });
  return res;
}

export {
  uploadImage
}