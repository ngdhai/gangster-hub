<center>
<a href="https://gitlab.com/ngdhai/gangster-hub/commits/master">
  <img src="https://gitlab.com/ngdhai/gangster-hub/badges/master/pipeline.svg">
</a>
</center>


# <span style="color: #C21D4F; letter-spacing: 1px">Lời nói đầu</span>
### Đây là báo cáo cho đồ án giữa kỳ môn <span style="color: #E862AC">Công nghệ mới trong phát triển phần mềm</span>

# <span style="color: #C21D4F; letter-spacing: 1px">Thông tin</span>
### Người thực hiện: <span style="color: #FB7F30">Nguyễn Đình Hải</span> - 1512139
### Git: [Gitlab](https://gitlab.com/ngdhai/gangster-hub)
### Host: https://gangster-hub.firebaseapp.com
### Package manager: <span style="color: #2187B2">Yarn</span>

# <span style="color: #C21D4F; letter-spacing: 1px">Khởi động Project</span>
```
$ git clone https://gitlab.com/ngdhai/gangster-hub.git
```
```
$ cd gangster-hub
```
```
$ yarn
```
```
$ yarn start
```
<br/>
<br/>
<br/>



# <span style="color: #C21D4F; letter-spacing: 1px">Các công nghệ và package sử dụng</span>
* ## <span style="color: #69D9F9">React</span>
* ## <span style="color: #4354B0">Material UI</span>
* ## <span style="color: #7350B7">Redux</span>
* ## <span style="color: #E862AC">Redux Saga</span>
* ## <span style="color: #68C47B">React Router</span>
* ## <span style="color: #FB7F30">Firebase</span>
* ## <span style="color: #f5dd41">JSS</span>
* ## react-redux-firebase


# <span style="color: #C21D4F; letter-spacing: 1px">Tổ chức code</span>
![Imgur](https://i.imgur.com/mefwfT7.png)

* ## Ứng dụng chính: <span style="color: #F10857">App.js</span> - Thực hiện việc setup react-router, căn chỉnh layout, gọi saga tạo một số event channel để listen một số event từ Firebase.
*Gọi saga tạo event channel ở đây vì App chỉ render 1 lần và không rerender*
* ## Thư mục: <span style="color: #F10857">layouts</span> - Chứa 3 <span style="color: #68C47B">Component</span> giúp cố định vị trí và phần chia các <span style="color: #68C47B">Component</span> tạo nên ứng dụng.
  * Header.js - Chứa các <span style="color: #68C47B">Component</span> thuộc về phần Header của ứng dụng. Cụ thể trong project này là thanh Navigation Bar.
  * Main.js - Chứa các <span style="color: #68C47B">Component</span> thuộc về phần Main của ứng dụng. Cụ thể trong project này là các <span style="color: #68C47B">Component</span>: ChatBox, Login, SignUp (đồ án không yêu cầu đăng ký nên không thực hiện)
  * Footer.js - Chứa các <span style="color: #68C47B">Component</span> thuộc về phần Footer của ứng dụng. Trong project này bỏ trống.
  * index.js - Public API cho thư mục layouts

*3 <span style="color: #68C47B">Component</span> Header, Main, Footer là khung xương của ứng dụng, dùng để tổ chức các <span style="color: #68C47B">Component</span> con thành các thành phần dựa trên vị trí xuất hiện trên ứng dụng.*

* ## Thư mục: <span style="color: #F10857">components</span> - Chưa các <span style="color: #68C47B">Component</span> của ứng dụng
  * Mỗi thư mục con là mỗi <span style="color: #68C47B">Component</span>.
    * \<ten-component\>.presenter.js - Phần presenter cho <span style="color: #68C47B">Component</span>
    * \<ten-component\>.styles.js - Phần styling cho <span style="color: #68C47B">Component</span>. *Project này sử dụng JSS để thực hiện styling*
    * index.js - Phần Container cho <span style="color: #68C47B">Component</span>
* ## Thư mục: <span style="color: #F10857">actions</span> - Chứa <span style="color: #FB7F30">Action Types</span>, <span style="color: #FB7F30">Action Creators</span> của <span style="color: #7350B7">Redux</span>

* ## Thư mục: <span style="color: #F10857">selectors</span> - Chứa các <span style="color: #FB7F30">Selectors</span> để lấy các dữ liệu cần thiết từ state.

* ## Thư mục: <span style="color: #F10857">reducers</span> - Chứa các <span style="color: #FB7F30">Reducers</span> của <span style="color: #7350B7">Redux</span>.

* ## Thư mục: <span style="color: #F10857">store</span> - Chứa phần setup cho <span style="color: #FB7F30">Redux Store</span>.

* ## Thư mục: <span style="color: #F10857">sagas</span> - Chứa các <span style="color: #FB7F30">Saga</span> xử lý một side effect trong việc tương tác với firebase. Note: *Project có sự trộn lẫn phần tương tác với <span style="color: #FB7F30">Firebase</span> bằng react-redux-firebase và tương tác trực tiếp qua <span style="color: #E862AC">Redux Saga</span>. Các tính năng đăng nhập và quản lý user sẽ sử dụng react-redux-firebase còn các tính năng liên quan đến phần chat sẽ được xử lý qua <span style="color: #E862AC">Redux Saga</span>*.
  * index.js - chứa rootSaga watch các action cần xử lý side effect.
  * *.js - chứa các handler cho các action được xử lý bởi <span style="color: #E862AC">Redux Saga</span>.

* ## Thư mục: <span style="color: #F10857">firebase-api</span> - <span style="color: #FB7F30">Firebase</span> config và các function gọi các API cập nhật dữ liệu và lấy dữ liệu.

<br/>
<br/>
<br/>
<br/>
<br/>

# <span style="color: #C21D4F; letter-spacing: 1px">Bảng đánh giá</span>
|ID|Yêu cầu|Điểm|
|:---:|:---:|:---:|
|1|Xây dựng giao diện|1|
|2|Cho phép sử dụng Google để đăng nhập| 1 |
|3|Hiện thị người trong hệ thống| 1 |
|4|Nhấn vào một người hiển thị giao diện cho phép xem lịch sử chat và chat với người đó | 1 |
|5|Cho phép Star một người, online hiện người này ở đầu danh sách | 1 |
|6| Cho phép search theo tên | 1|
|7| Cho phép gửi image link và hiện trong khung chat | 1 |
|8| Cho phép upload hình và hiện hình đó sau khi gửi tin | 1 |
|9| Cấu trúc project bài bản, hợp lý | 1 |
|10| Đăng tải lên firebase | 1|

#### Tổng điểm: 10